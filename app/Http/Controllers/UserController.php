<?php

namespace DnRExam\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Session;
use DnRExam\User;

class UserController extends Controller
{
    public function register(Request $req){

        $user = User::where('email', $req->input('email'))->get();

        if(count($user)){
            $json['status'] = 'error';
            $json['message'] = 'Email Address is already been used!';
        }else{
            $data = array(
                'email' => $req->input('email'),
                'password' => bcrypt($req->input('password')),
                'first_name' => $req->input('fname'),
                'last_name' => $req->input('lname'),
                'address' => $req->input('address'),
                'phone' => $req->input('phone'),
                'photo' => ''
            );
            $json = array();
            if(User::create($data)){
                $json['status'] = 'success';
                $json['message'] = 'Registered Successfully!';
            }
        }

        echo json_encode($json); 
    }

    public function login(Request $req){
        $email = $req->input('login_email');
        $password = $req->input('login_password');
        $user = User::whereEmail($email)->first();
        $found = false;
        if(count($user)){
            $found = (Hash::check($password, $user['password']))?TRUE:FALSE;
        }else{
            $found = false;
        }
        
        if($found){
            $user_data = array('email'=>$user['email'], 'name' => ucwords($user['first_name']).' '.ucwords($user['last_name']), 'user_id' => $user['id'], 'loggedin'=>true);
            Session::put('user_data', $user_data);
            return redirect('customers');
        }else{
            $user_data = array('loggedin' => false);
            Session::put('user_data', $user_data);
            Session::flash('login_message', 'Username or password is incorrect!');
            return redirect('/');
        }

        ;
    }

}
