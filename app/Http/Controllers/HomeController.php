<?php

namespace DnRExam\Http\Controllers;

use Illuminate\Http\Request;
use Session;
class HomeController extends Controller
{
    //
    public function index(Request $request){
        return view('home.landing');
    }
    
}
