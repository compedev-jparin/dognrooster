<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as'=>'home', 'uses' => 'HomeController@index']);

Route::group(['prefix' => 'users'], function(){

    Route::get('edit/{id}', function($id){
        
    });

    Route::get('/logout', function(){
        Session::flush();
        return redirect('/');
    });

    Route::post('registration', [
        'uses' => 'UserController@register'
    ]);

    Route::post('login', [
        'uses' => 'UserController@login'
    ]);
    Route::get('login', function(){
        return redirect('/');
    });
});

Route::group(['prefix' => 'customers'], function(){

    Route::get('/', function(){
        return view('customers.index');        
    });

    Route::post('get', [
        'uses' => 'CustomerController@getAll'
    ]);

    Route::post('add', [
        'uses' => 'CustomerController@register'
    ]);

    Route::post('edit', [
        'uses' => 'CustomerController@login'
    ]);

});

