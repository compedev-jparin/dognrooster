$(function() {

    // HOMEPAGE MODAL
    $('#login-form-link').click(function(e) {
		$("#login_form").delay(100).fadeIn(100);
 		$("#register_form").fadeOut(100);
		$('#register-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#register-form-link').click(function(e) {
		$("#register_form").delay(100).fadeIn(100);
 		$("#login_form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});

    // REGISTRATION
    var reg_rules = {
        errorElement: 'span',
        rules : {
            email : {
                email : true,
                required: true
            },
            password : {
                required : true,
                minlength: 6
            },
            confirm_password : {
                equalTo: "#password"
            },
            fname : "required",
            lname : "required",
            address : "required",
            phone : "required",
        }
    }

     $('#register_form').validate(reg_rules);

    $('#register_submit').click(function(){
        var reg_btn = $(this);
        if($('#register_form').valid()){
            reg_btn.ladda();
            reg_btn.ladda('start');
            reg_btn.prop('disabled', true);
            user.register(function(result){
                if(result.status == 'success'){
                    $('#reg_message').html('<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert">&times;</a><strong></strong>'+result.message+'.</div>');
                    $('input').val('');
                }else{
                     $('#reg_message').html('<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert">&times;</a><strong></strong>'+result.message+'.</div>');
                }
                reg_btn.prop('disabled', false);
                reg_btn.ladda('stop');
            });
        }
        // return false;
    });

    customer.dataTableInit();
});



var user = {
    register : function(cb){

        var ajaxUserRegistration = {
            url : url.base+"/users/registration",
			type : "post",
			dataType : "json",
			data :{
                _token : $('#register_form input[name="_token"]').val(),
                fname : $('#fname').val(),
                lname : $('#lname').val(),
                email : $('#email').val(),
                password : $('#password').val(),
                address : $('#address').val(),
                phone : $('#phone').val()
			},success : function(response){
				cb(response);
			},
			error : function(response){
				console.log(response);
			}
        }
        $.ajax(ajaxUserRegistration);
    }
}


var customer = {
    dataTableInit : function(){
		$('.customersTable').DataTable({
			"aaSorting": [[0, "asc"]],
			"iDisplayLength": 10,
			"columnDefs": [{
				"targets": 1,
				"orderable": false
			}],
			"autoWidth": false,
			"destroy" : true,
			"processing": true,
			"serverSide": true,
			// "stateSave" : true,
			"ajax": {
				"url": url.base+"/customers/get",
				"type": "POST",
				"data" : function(d){
				}
			},
			"fnInitComplete": function (oSettings, json) {
				
			}
		});

    }
}