@extends('template.default')

@section('content')


<div class="container">

        <h1 class="text-center" style="margin-bottom:20px;">Dog N Rooster Exam</h1>
    	<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-6">
								<a href="#" class="active" id="login-form-link">Login</a>
							</div>
							<div class="col-xs-6">
								<a href="#" id="register-form-link">Register</a>
							</div>
						</div>
						<hr>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<form id="login_form" action="/users/login" method="post" role="form" style="display: block;">
									@if(Session::has('login_message'))
									<div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert">&times;</a><strong></strong>{!! session('login_message') !!}</div>
									@endif
									

                                    {{ csrf_field() }}
									<div class="form-group">
										<input type="text" name="login_email" id="login_email" tabindex="1" class="form-control" placeholder="Username" value="">
									</div>
									<div class="form-group">
										<input type="password" name="login_password" id="login_password" tabindex="2" class="form-control" placeholder="Password">
									</div>
									<div class="form-group text-center hide">
										<input type="checkbox" tabindex="3" class="" name="remember" id="remember">
										<label for="remember"> Remember Me</label>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row hide">
											<div class="col-lg-12">
												<div class="text-center">
													<a href="https://phpoll.com/recover" tabindex="5" class="forgot-password">Forgot Password?</a>
												</div>
											</div>
										</div>
									</div>
								</form>

								<form id="register_form" action="" method="post" role="form" style="display: none;">
                                    <div id="reg_message"></div>
                                    {{ csrf_field() }}
									<div class="form-group">
										<input type="email" name="email" id="email" tabindex="1" class="form-control" placeholder="Email Address" value="">
									</div>
									<div class="form-group">
										<input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password">
									</div>
									<div class="form-group">
										<input type="password" name="confirm_password" id="confirm_password" tabindex="3" class="form-control" placeholder="Confirm Password">
									</div>
                                    <div class="form-group">
										<input type="text" name="fname" id="fname" tabindex="4" class="form-control" placeholder="First Name" value="">
									</div>
                                    <div class="form-group">
                                        <input type="text" name="lname" id="lname" tabindex="5" class="form-control" placeholder="Last Name" value="">
                                    </div>
                                    <div class="form-group ">
										<input type="text" name="address" id="address" tabindex="6" class="form-control" placeholder="Address">
									</div>
                                    <div class="form-group">
										<input type="text" name="phone" id="phone" tabindex="7" class="form-control" placeholder="Phone No.">
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
									  
                                                <button name="register_submit" id="register_submit" tabindex="8" class="form-control btn btn-register ladda-button" type="button" data-style="zoom-in"><span class="ladda-label">Submit</span></button>
                                                
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection