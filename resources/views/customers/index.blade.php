@extends("template.default")

@section('lib_css')
<link href="{!! asset('css/libs/dataTables/datatables.min.css') !!}" media="all" rel="stylesheet" type="text/css" />
@endsection

@section('lib_js')
<script type="text/javascript" src="{!! asset('js/libs/dataTables/datatables.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('js/libs/bootbox/bootbox.js') !!}"></script>
@endsection

@section('content')
<div class="container">
    <div class="row headline">
        <div class="col-sm-6">
        <h4>Welcome! {!! Session::get('user_data')['name'] !!}</h4>
        </div>
        <div class="col-sm-6 text-right">
        <a class="btn btn-primary">logout</a>
        </div>
    </div>
    <h1>Customers</h1>
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-content">
                        <div class="table-responsive">
                            <table class="table table-hover customersTable">
                                <thead>
                                    <tr>
                                        <th>Customer ID</th>
                                        <th>Email</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Address</th>
                                        <th>Phone</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection