<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="{!! asset('js/libs/jquery.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('js/libs/jquery.validate.js') !!}"></script>
        <!-- Bootstrap CDN -->
            <!-- Latest compiled and minified CSS -->
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

            <!-- Latest compiled and minified JavaScript -->
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <!-- LADDA -->
      
        
        <script type="text/javascript" src="{!! asset('js/libs/ladda/spin.min.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('js/libs/ladda/ladda.min.js') !!}"></script>
        <script type="text/javascript" src="{!! asset('js/libs/ladda/ladda.jquery.min.js') !!}"></script>
        <link href="{!! asset('css/libs/ladda/ladda.themeless.css') !!}" media="all" rel="stylesheet" type="text/css" />
        @yield('lib_css')
        @yield('lib_js')

        <link href="{!! asset('css/style.css') !!}" media="all" rel="stylesheet" type="text/css" />
    </head>
    <body>
        @yield('content')

         <script>
             var url= {
                base :  "<?=url('/');?>" 
             }
         </script>
         <script type="text/javascript" src="{{ URL::asset('js/all.js') }}"></script>
         @yield('custom_js')
    </body>
</html>
